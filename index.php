<!doctype html>
<html lang="en"><head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-60S38R0Z3C"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-60S38R0Z3C');
	</script>

	<meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Asistente Virtual</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>

	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="assets/css/demo.css" rel="stylesheet" />
	
	<!-- whasapp    -->

	<link rel="stylesheet" href="./assets/floating-whatsapp/floating-wpp.min.css">

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/nucleo-icons.css" rel="stylesheet">

</head>
<body>
 
		<div class="page-header" data-parallax="true" style="background-image: url('assets/img/home.jpg');" >
			 
			<nav class="navbar navbar-expand-lg fixed-top navbar-transparent" >
				<div class="container">
					<div class="navbar-translate">
						<a class="navbar-brand" href="http://analiza.xyz">
							<img  src="./assets/img/logo3.png" >

							</img>
						</a>
						<button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-bar"></span>
							<span class="navbar-toggler-bar"></span>
							<span class="navbar-toggler-bar"></span>
						</button>
					</div>
				   <div class="collapse navbar-collapse" id="navbarToggler">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a href="../inmobiliere/public/login" class="nav-link"><i class="nc-icon nc-layout-11"></i>Iniciar Sesión</a>
							</li>
							<!--<li class="nav-item">
								<a href="../documentation/tutorial-components.html" target="_blank" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>  Documentation</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
									<i class="fa fa-twitter"></i>
									<p class="d-lg-none">Twitter</p>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
									<i class="fa fa-facebook-square"></i>
									<p class="d-lg-none">Facebook</p>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
									<i class="fa fa-instagram"></i>
									<p class="d-lg-none">Instagram</p>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" rel="tooltip" title="Star on GitHub" data-placement="bottom" href="https://www.github.com/CreativeTimOfficial" target="_blank">
									<i class="fa fa-github"></i>
									<p class="d-lg-none">GitHub</p>
								</a>
							</li> -->
						</ul>
					</div>
				</div>
			</nav>
			<nav>&nbsp;</nav>
		  <div class="filter"></div>
			<div class="container">
			    <div class="motto text-center">
			        <h1>Software para empresas</h1>
			        <h3>Te ayudamos a optimizar procesos.</h3>
			        <br />
			        <!--<a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" class="btn btn-outline-neutral btn-round"><i class="fa fa-play"></i>Watch video</a>
			        <button type="button" class="btn btn-outline-neutral btn-round">Download</button>-->
			    </div>
			</div>
    	</div>
        <div class="main">
			<div class="section text-center">
            <div class="container">
				<div id="myDiv"></div>
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
						
                        <h2 class="title">Sistema de Gestión Empresarial</h2>
                        <h5 class="description">Tenemos soluciones que se pueden adaptar en tu organización con el fin de mejorar los procesos que dependen de la eficiente utilización de la información. Tu escoges que servicios necesitas.</h5>
                        <br>
                       <!-- <a href="#paper-kit" class="btn btn-danger btn-round">See Details</a> -->
                    </div>
                </div>
				<br/><br/>
				<div class="row">
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-satisfied"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Gestión de clientes</h4>
								<p class="description">Organiza la información de tus clientes que ayude a la gestión comercial y genera documentos rapidamente.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-money-coins"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Recibos de pago</h4>
								<p>Almacena información de tus clientes y emite recibos de pago con código de barras para recaudo bancario.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-chart-bar-32"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Gestión de Cartera</h4>
								<p>Controla los pagos de los clientes y ayuda en la recuperación de la cartera de morosos.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-spaceship"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Aplicativo web</h4>
								<p>Tienes acceso a tu información las 24 horas del día por medio de tus dispositivos conectados a internet como el celular o compuatador.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
                </div>
                <div class="row"><hr></div>
                <div class="row">
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-cloud-download-93"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Almacenamiento de archivos</h4>
								<p class="description">Cuenta con administración y almacenamiento seguro en la nube de archivos.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-chart-pie-36"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Estadisticas y alertas</h4>
								<p>Procesa información y genera informes de gestión como tambien recibe alertas programadas que ayuden a recordar tareas.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-email-85"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Envio de correo a clientes</h4>
								<p>Genera comunicaciones con tus clientes por medio de correos electronicos con información de interes.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info">
							<div class="icon icon-danger">
								<i class="nc-icon nc-istanbul"></i>
							</div>
							<div class="description">
								<h4 class="info-title">Asistente Inmobiliario</h4>
								<p>Ayuda a organizar información de inmuebles y emitir documentos de gestión inmobiliaria como extractos de arriendo y contratos.<BR></p>
								<!--<a href="#pkp" class="btn btn-link btn-danger">VER MAS</a> -->
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>

		<!--<div class="section section-dark text-center">
            <div class="container">
                <h2 class="title">Let's talk about us</h2>
				<div class="row">
    				<div class="col-md-4">
                        <div class="card card-profile card-plain">
                            <div class="card-avatar">
                                <a href="#avatar"><img src="../assets/img/faces/clem-onojeghuo-3.jpg" alt="..."></a>
                            </div>
                            <div class="card-body">
                                <a href="#paper-kit">
                                    <div class="author">
                                        <h4 class="card-title">Henry Ford</h4>
                                        <h6 class="card-category">Product Manager</h6>
                                    </div>
                                </a>
                                <p class="card-description text-center">
                                Teamwork is so important that it is virtually impossible for you to reach the heights of your capabilities or make the money that you want without becoming very good at it.
                                </p>
                            </div>
                            <div class="card-footer text-center">
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
    				</div>

    				<div class="col-md-4">
                        <div class="card card-profile card-plain">
                            <div class="card-avatar">
                                <a href="#avatar"><img src="../assets/img/faces/joe-gardner-2.jpg" alt="..."></a>
                            </div>
                            <div class="card-body">
                                <a href="#paper-kit">
                                    <div class="author">
                                        <h4 class="card-title">Sophie West</h4>
                                        <h6 class="card-category">Designer</h6>
                                    </div>
                                </a>
                                <p class="card-description text-center">
                                A group becomes a team when each member is sure enough of himself and his contribution to praise the skill of the others. No one can whistle a symphony. It takes an orchestra to play it.
                                </p>
                            </div>
                            <div class="card-footer text-center">
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
    				</div>

    				<div class="col-md-4">
                        <div class="card card-profile card-plain">
                            <div class="card-avatar">
                                <a href="#avatar"><img src="../assets/img/faces/erik-lucatero-2.jpg" alt="..."></a>
                            </div>
                            <div class="card-body">
                                <a href="#paper-kit">
                                    <div class="author">
                                        <h4 class="card-title">Robert Orben</h4>
                                        <h6 class="card-category">Developer</h6>
                                    </div>
                                </a>
                                <p class="card-description text-center">
                                The strength of the team is each individual member. The strength of each member is the team. If you can laugh together, you can work together, silence isn’t golden, it’s deadly.
                                </p>
                            </div>
                            <div class="card-footer text-center">
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-twitter"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-google-plus"></i></a>
                                <a href="#pablo" class="btn btn-link btn-just-icon btn-neutral"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
    				</div>
    			</div>
        	</div>
    	</div>

            <div class="section section-dark landing-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="title">ENVIANOS UN MENSAJE</h2>
                            <form class="contact-form" action="/controller/email.php" method="POST" >
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="text-light">Nombre</label>
										<div class="input-group">
	                                        <span class="input-group-addon">
	                                            <i class="nc-icon nc-single-02"></i>
	                                        </span>
	                                        <input name="Name" type="text" class="form-control" placeholder="Name">
	                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-light">Email</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="nc-icon nc-email-85"></i>
											</span>
											<input name="Email" type="text" class="form-control" placeholder="Email">
										</div>
                                    </div>
                                </div>
                                <label class="text-light">Mensaje</label>
                                <textarea name="Message" class="form-control" rows="4" placeholder="cuentanos en que te podemos ayudar !"></textarea>
                                <div class="row">
                                    <div class="col-md-4 ml-auto mr-auto">
                                        <button class="btn btn-danger btn-lg btn-fill">Enviar Mensaje</button>
                                    </div>
                                </div>
                            </form>
						
                        </div>
						
                    </div>
                </div>
            </div>-->
        </div>
<footer class="footer section-dark">
		<div class="container">
			<div class="row">
				<nav class="footer-nav">
					<ul>
						
						<li>
							<span class="input-group-addon"><i class="nc-icon nc-send"></i> <a > &nbsp; whatsapp: +57 3193964010</a>
											</span>
							</li>
					
				
					</ul>
				</nav>
				<div class="credits ml-auto">
					<span class="copyright">
						© <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> 
					</span>
				</div>
			</div>
		</div>
	</footer> 
	



</body>

<!-- Core JS Files -->

<script src="assets/js/jquery-3.2.1.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<script src="assets/js/popper.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="./assets/floating-whatsapp/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="./assets/floating-whatsapp/floating-wpp.min.js"></script>

<script type="text/javascript">
  $(function () {   
    $('#myDiv').floatingWhatsApp({
      	phone: '573193964010',
		popupMessage: 'Hola, ¿en que podemos ayudarte?',
    	showPopup: true
    });
  });
</script>

<!--  Paper Kit Initialization snd functons -->
<script src="assets/js/paper-kit.js?v=2.1.0"></script>

</html>
